var $ = require('jQuery')

$(document).ready(function () {
  document.addEventListener('mousewheel', function(e) {
    if(e.deltaY % 1 !== 0) {
      e.preventDefault();
    }
  });

  function touchHandler(event) {
    if(event.touches.length > 1){
        event.preventDefault()
    }
  }

  $('.close-button').click(function () {
    $('.modal').hide()
  })

  $('#conference_map').click(function () {
    $('.modal').css('display', 'flex')
    $('.modal').find('.schedule-item-details').hide()
    $('.modal').find('.maps').show()
    $('.modal').find('.photo').hide()
  })
})
