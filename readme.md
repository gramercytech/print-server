# Up and Running

$ `git clone git@bitbucket.org:gramercytech/print-server.git`

$ `cd print-server`

$ `npm install`

Add a `.env` file in the root folder if one does not exist and copy over the contents from the `.env.example` file. Fill in the pusher keys and cluster values

$ `npm start`

# Tips

- To test locally without a real printer, you can comment out the checks for a selected printers in `print-server.js@printAndPreview`
- You will need to manually set the DEV pusher keys in `pusher.js` and the `DEVBaseURL` in `routes.js` if you wish to test on the develop environment
- Uncomment `mainWindow.webContents.openDevTools()` in `main.js` to run the app locally with the debug console