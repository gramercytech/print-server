// Modules to control application life and create native browser window
const {app, BrowserWindow, screen} = require('electron')
const path = require('path')

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow

// Create the browser window.
function createWindow() {
  const { width, height } = screen.getPrimaryDisplay().workAreaSize
  const iconPath = path.join(__dirname, './assets/icons/png/512x512.png');
  if (app.dock) app.dock.setIcon(iconPath)

  mainWindow = new BrowserWindow({
    width: width,
    height: height,
    title: 'Badge Print Server',
    icon: path.join(__dirname, 'assets/icons/png/64x64.png'),
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false
    }
  })

  require('@electron/remote/main').enable(mainWindow.webContents)

  const urlPath = `file://${path.join(__dirname, "login.html")}`

  mainWindow.loadURL(urlPath)

  //mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })
}


// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
  require('@electron/remote/main').initialize()

  createWindow()

  app.on('activate', function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })
})


// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})
