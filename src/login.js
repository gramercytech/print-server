'use strict'
const $ = require('jQuery')
const routes = require('./routes.js');
const packageJson = require('../package.json')

function setServer() {
  let server = $('#server').val();
  let localUrl =  $('#local_url').val()
  window.localStorage.setItem('server', server);
  window.localStorage.setItem('local_url', localUrl);
}

function makeUniqueID(length) {
  var result = '';
  var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

$(document).on('change', '#server', function () {
  if ($(this).val() === 'local') {
    $('#local-url-field').show()
  } else {
    $('#local-url-field').hide()
  }
})

function login (credentials) {
  var loginUrl = routes.login()

  if (credentials.email == "" || credentials.password == "" ) {
    $('.login-error').text('Please enter a valid email and password')
    return
  }

  window.fetch(loginUrl, {
    method: 'post',
    body: JSON.stringify(credentials),
    headers: {
      'Content-type': 'application/json'
    }
  }).then(function (response) {
    response.json().then(function (res) {
      if (response.status !== 200) {
        if (res.data == "mfa_required") {
          $('.login-error').text("MFA Required - Please check your authenticator app for the code")
          
          $('input[name="email"]').hide();
          $('input[name="password"]').hide();
          $('#server-select').hide();
          
          $('#cancel').show()
          $('#mfa-text-field').show()
          return
        }

        if (res.message == "Invalid 2 Factor Code") {
          $('.login-error').text("Invalid MFA Code")
          return
        }

        $('.login-error').text(res.message)
        return
      }

      $('.login-error').text('')
      window.localStorage.setItem('authToken', res.data.api_auth_token)
      window.localStorage.setItem('adminEmail', res.data.email)
      window.localStorage.setItem('printServerId', `${makeUniqueID(6)}`)
      window.location.replace('index.html')
    }).catch(function (err) {
      console.log('Fetch Error :-S', err)
    })
  })
}

$(document).ready(function () {
  if (window.localStorage.getItem('authToken') != null) {
    window.location.replace('index.html')
  }

  $('#app-version').html(`(v${packageJson.version})`); // Set Version

  $( "#cancel" ).click(function() {
    window.location.replace('login.html')
  });

  $('form').submit((event) => {
    event.preventDefault()

    setServer()

    let email = $('input[name="email"]').val()
    let password = $('input[name="password"]').val()
    let code = $('input[name="mfa_code"]').val()
    login({ email: email, password: password, mfa_code: code })
  })
})
