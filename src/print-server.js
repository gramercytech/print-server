const WebSocket = require('ws')
const { BrowserWindow } = require('@electron/remote')
const os = require('os')
const ifaces = os.networkInterfaces()
const puppeteer = require('puppeteer');
const { exec } = require('child_process')
const fs = require('fs');
const tmp = require('tmp');
const path = require('path');
window.$ = window.jQuery = require('jquery');
require('select2')(window.jQuery);

const pdfToPrinter = require('pdf-to-printer');
const pdfToPrinterExePath = path.join(__dirname,  '..', 'packages', 'PDFtoPrinter.exe');
const env = require('dotenv').config();
const routes = require('./routes.js');
const pusher = require('./pusher.js');
const packageJson = require('../package.json')

module.exports = function () {
  let badgePrinter = {
    availablePrinters: getAvailablePrinters(),
    targetPrinters: [],
    currentPrinter: null,
    events: [],
    eventId: null,

    getPrintUrl: function (eventAttendeeId) {
      const eventId = window.localStorage.getItem('eventId')
      const token = window.localStorage.getItem('authToken')
      return routes.attendeeBadge(eventId, eventAttendeeId, token)
    },

    getEventsListUrl: function () {
      const token = window.localStorage.getItem('authToken')
      return routes.events(token)
    },

    printPDF: async function (eventAttendeeIds, printUrl, deviceName) {
      console.log('Printing as PDF')
      const chromiumExecutablePath = path.join(__dirname, '..', '.cache', 'puppeteer', 'chrome', 'win64-114.0.5735.133', 'chrome-win64', 'chrome.exe')

      // Launch Puppeteer and open a new page
      const browser = await puppeteer.launch({
        headless: "new",
        executablePath: chromiumExecutablePath,
      });

      const page = await browser.newPage();
      // Navigate to the URL
      await page.goto(printUrl, { waitUntil: 'networkidle0' });
      // Refresh the page to fix resizing issue
      //await page.reload({ waitUntil: 'networkidle0' });
      // Wait for 3 seconds after the page is reloaded
      //await page.waitForTimeout(3000);
      // Create a PDF from the loaded page
      const pdfBuffer = await page.pdf({
        format: 'A4',
        printBackground: true,
        margin: { top: 0, right: 0, bottom: 0, left: 0 },
      });

      // Create a subdirectory within Temp directory
      const tempDirPath = path.join(os.tmpdir(), 'printpdfs');
      if (!fs.existsSync(tempDirPath)) {
        fs.mkdirSync(tempDirPath);
      }

      // Create a unique temp directory
      const uniqueTempDir = fs.mkdtempSync(path.join(tempDirPath, 'pdf-'));
      // Create a unique filename for the PDF
      const tmpPdfPath = path.join(uniqueTempDir, `${Date.now()}.pdf`);

      fs.writeFileSync(tmpPdfPath, pdfBuffer);

      // Print the PDF file in the background
      const printCommand = `"${pdfToPrinterExePath}" "${tmpPdfPath}" "${deviceName}"`;

      exec(printCommand, (error, stdout, stderr) => {
        if (error) {
          console.error('Error printing PDF:', error);
        } else {
          console.log('Finished print job')
          // Delete the temporary PDF file after it has been printed
          fs.unlinkSync(tmpPdfPath);
            // Delete the unique directory after the file has been deleted
            fs.rmdirSync(uniqueTempDir);
          }
        })

      // Close the browser
      await browser.close();
    },

    printWeb: function (eventAttendeeIds, printUrl, deviceName) {
      console.log('Printing via web')
      let win = new BrowserWindow({show: false})
      win.loadURL(printUrl)
      win.webContents.on('did-finish-load', () => {
        setTimeout(function () {
          win.webContents.print({
            deviceName: deviceName,
            printBackground: true,
            marginsType: 0,
            silent: true
          }, function () {
            console.log('finished print job')
          })
        }, 3000)
      })
    },

    print: async function (eventAttendeeIds) {
      this.getNextPrinter();
      const isPDFEnabled = $('#pdf-print-toggle').is(':checked');
      let printUrl = this.getPrintUrl() + '&eventAttendeeIds=' + eventAttendeeIds;
      const deviceName = this.targetPrinters[this.currentPrinter];

      if (isPDFEnabled) {
        this.printPDF(eventAttendeeIds, printUrl, deviceName);
      } else {
        this.printWeb(eventAttendeeIds, printUrl, deviceName);
      }
    },

    preview: function (eventAttendeeIds) {
      let printUrl = this.getPrintUrl(eventAttendeeIds)
      $('#badge').attr('src', printUrl)
    },

    printAndPreview: function (eventAttendeeIds) {
      if (!this.eventId) $('#error-message').text('You must select an event before printing')
      if (this.targetPrinters.length < 1) $('#error-message').text('You must select at least one target printer')
      if (!this.eventId || this.targetPrinters.length < 1) {
        $('.error-container').addClass('text-fade')
        setTimeout(function () {
          $('.error-container').removeClass('text-fade')
          $('#error-message').text('')
        }, 3000)
      } else {
        this.print(eventAttendeeIds)
        this.preview(eventAttendeeIds)
      }
    },

    getNextPrinter: function () {
      this.currentPrinter = this.currentPrinter >= (this.targetPrinters.length - 1)
        ? 0
        : this.currentPrinter + 1
    },

    setEventId: function (id) {
      this.eventId = id
    }
  }

  $('form').submit(function (event) {
    event.preventDefault()
    const eventAttendeeIds = $('#eventAttendeeIds').val()
    badgePrinter.printAndPreview(eventAttendeeIds)
  })

  $('.tabs ul li').click(function () {
    let section = $(this).data('section')
    if (section === 'sign-out') {
      window.localStorage.removeItem('eventId')
      window.localStorage.removeItem('authToken')
      window.location.replace('login.html')
    }
    $('.tabs ul li').removeClass('is-active')
    $(this).addClass('is-active')
    $('section').addClass('hidden')
    $('#' + section).removeClass('hidden')
  })

  $(document).on('change', '#available-printers input', function () {
    let printerName = $(this).parent().text()
    if ($(this).prop('checked')) {
      badgePrinter.targetPrinters.push(printerName)
    } else {
      badgePrinter.targetPrinters = badgePrinter.targetPrinters.filter((printer) => {
        return printer !== printerName
      })
    }
  })

  function delay(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  function getAvailablePrinters() {
    BrowserWindow.getFocusedWindow().webContents.getPrintersAsync()
      .then(printers => {
        let printerNames = printers.map((printer) => {
          return printer.name
        })
        createPrinterCheckboxes(printerNames)
        return printerNames
      })
  }

  function createPrinterCheckboxes(printers) {
    printers.forEach(function (printer) {
      let label = $('<label/>').addClass('checkbox').addClass('has-text-white-ter')
      let input = $('<input/>').attr('type', 'checkbox')
      let span = $('<span/>').addClass('checkmark')
      var textNode = document.createTextNode(printer)
      label.append(textNode)
      label.append(input)
      label.append(span)
      $('#available-printers').append(label)
    })
  }

  function initWSServer() {
    const wss = new WebSocket.Server({ port: 8080 })
    wss.on('connection', function connection(ws) {
      ws.on('message', function incoming(message) {
        handleDataFromWebsocket(message)
      })
      ws.send('something')
    })
  }

  function configurePusher () {
    pusher.configure(function(state){
      if(state.current == "connected") {
        $("#connection-status-offline").addClass('hidden')
      } else {
        $("#connection-status-offline").removeClass('hidden')
      }
    })
  }

  function handleDataFromWebsocket(message) {
    $('#manual form input').val(message)
    $('#manual form button').click()
  }

  function getIps() {
    Object.keys(ifaces).forEach(function (ifname) {
      var alias = 0
      ifaces[ifname].forEach(function (iface) {
        if (iface.family !== 'IPv4' || iface.internal !== false) {
          // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
          return
        }

        if (alias >= 1) {
          // this single interface has multiple ipv4 addresses
          console.log(ifname + ':' + alias, iface.address)
          let container = createIpContainer(ifname, iface.address)
          $('#device-ips').append(container)
        } else {
          // this interface has only one ipv4 adress
          console.log(ifname, iface.address)
          let container = createIpContainer(ifname, iface.address)
          $('#device-ips').append(container)
        }
        ++alias
      })
    })
  }

  function createIpContainer(ifname, address) {
    let container = $('<div/>')
    let ifNameString = `<h4 class="is-size-4" style="display: inline-block;">${ifname}</h4>`
    let addressString = `<span>&nbsp; - ${address}</span>`
    container.html(`${ifNameString}${addressString}`)
    return container
  }

  function setUpData() {
    const email = window.localStorage.getItem('adminEmail')
    const env = window.localStorage.getItem('server')
    const uid = window.localStorage.getItem('printServerId')
    
    $(document).ready(function() {
      $('#current-admin-email').html(email)
      $('#current-env').html(`(${env})`)
      $('#unique-print-server-id').html(uid)
      $('#app-version').html(`Version: ${packageJson.version}`)
    })
  }

  function subscribeToEventPusher() {
    $("#connection-message").addClass('hidden')
    $('#connection-options').removeClass('hidden')

    const eventId = window.localStorage.getItem('eventId')
    const uid = window.localStorage.getItem('printServerId')
    pusher.subscribe(eventId, uid, function(data) {
      console.log(data)
      const eid = data.message.event_attendee_id
      badgePrinter.printAndPreview(eid)
    })
  }

  function getEventList () {
    let eventsUrl = badgePrinter.getEventsListUrl()

    window.fetch(eventsUrl, {
      method: 'get',
      headers: {
        'Content-type': 'application/json'
      }
    }).then(function (response) {
      if (response.status !== 200) {
        console.log('Looks like there was a problem. Status Code: ' + response.status)
        return
      }
      response.json().then(function (res) {
        badgePrinter.events = setEvents(res.data)
      }).catch(function (err) {
        console.log('Fetch Error :-S', err)
      })
    })
  }

  function setEvents(events) {
    events.map((ev) => {
      return { id: ev.id, name: ev.name }
    })

    const selectedEventId = window.localStorage.getItem('eventId')

    events.forEach((ev) => {
      let option = $('<option/>')
      let eventId = ev.id ?? ev.event_id

      option.text(ev.name)
      option.attr('value', eventId)

      if (selectedEventId != null && eventId == selectedEventId) {
        option.attr('selected', 'selected')
        subscribeToEventPusher()
      }
      $('#event-selector').append(option)
    })
    setEventSelectListeners()
    return events
  }

  function setEventSelectListeners () {
    $('#event-selector').select2().on('select2:open', function(e){
        $('.select2-search__field').attr('placeholder', 'Enter event...');
    })

    $('#event-selector').change((event) => {
      const eventId = $('#event-selector').find(':selected').val()
      window.localStorage.setItem('eventId', eventId)
      badgePrinter.eventId = eventId
      subscribeToEventPusher()
    })
  }

  setUpData()
  getEventList()
  getIps()
  initWSServer()
  configurePusher()

  return badgePrinter
}