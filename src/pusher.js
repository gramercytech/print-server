const CONFIG = {
  AWS_PROD: {
    KEY: "51c5eb05643109bedd27",
    CLUSTER: "mt1"
  },
  GCP_PROD: {
    KEY: "b735e6df024a3b04aa9c",
    CLUSTER: "us2"
  },
  GCP_DEV: {
    KEY: "964d8139bf0d08cfbaba",
    CLUSTER: "us2"
  }
};

const Pusher = require('pusher-js');

function currentServer() {
  const server = window.localStorage.getItem('server')
  return server
}

let pusher = {
  instance: null,
  channelName: null,
  
  configure: function (callback) {
    let config = CONFIG.AWS_PROD
    if (currentServer() == "gcp") {
      config = CONFIG.GCP_PROD
    }
    
    //Uncomment to test on develop
    //config = CONFIG.GCP_DEV
    
    this.instance = new Pusher(config.KEY, {
      cluster: config.CLUSTER,
    });

    this.instance.connection.bind("state_change", function (states) {
      callback(states)
    });
  },

  subscribe: function (eventId, UID, callback) {
    const newChannelName =`print_servers.events.${eventId}.topic.${UID}`

    if (newChannelName !== this.channelName) {
      this.instance.unsubscribe(this.channelName);
      this.channelName = newChannelName
      let channel = this.instance.subscribe(newChannelName);
      
      channel.bind("App\\Events\\PrintMessageEvent", function(data) {
        callback(data)
      });
    }
  }
}

module.exports = pusher