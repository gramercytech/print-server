const AWSBaseURL = 'https://api2.eventfinity.co';
const GCPBaseURL = 'https://api.eventfinity.io';
const QABaseURL = 'https://api.qa.developfinity.com';
const DEVBaseURL = 'https://api.dev.developfinity.com';

function currentServer() {
    return window.localStorage.getItem('server')
}

function localBaseUrl() {
    return window.localStorage.getItem('local_url')
}

function baseURL() {
    switch (currentServer()) {
        case "gcp":
            return GCPBaseURL;
        case "qa":
            return QABaseURL;
        case "dev":
            return DEVBaseURL;
        case "local":
            return localBaseUrl();
        default:
            return AWSBaseURL; // Default to AWS if no specific server is matched
    }
}

const routes = {
    login: function() {
        return baseURL()+"/api/v1/admin/log_in"
    },
    events: function(token) {
        return `${baseURL()}/api/v1/admin/events-list?printServer=true&api_auth_token=${token}`
    },
    attendeeBadge: function(eventId, eventAttendeeId, token) {
        return `${baseURL()}/api/v1/admin/events/${eventId}/event_badges/printForAttendees?eventAttendeeIds=${eventAttendeeId}&api_auth_token=${token}`
    }
}

module.exports = routes
